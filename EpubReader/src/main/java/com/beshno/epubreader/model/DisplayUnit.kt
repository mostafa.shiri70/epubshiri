package com.beshno.epubreader.model

enum class DisplayUnit {
    PX,
    DP,
    CSS_PX
}