package com.beshno.epubreader.model.event;

import com.beshno.epubreader.ui.fragment.FolioPageFragment;

/**
 * This event is used when web page in {@link FolioPageFragment}
 * is to reloaded.
 */
public class ReloadDataEvent {
}
