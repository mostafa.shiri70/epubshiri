package com.beshno.beshnoEpub

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.beshno.epubreader.Config
import com.beshno.epubreader.Constants
import com.beshno.epubreader.FolioReader

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        epub()
    }

    private fun epub() {
        val folioReader = FolioReader.get()
        val config: Config = Config()
            .setDirection(Config.Direction.VERTICAL)
            .setFont(Constants.FONT_TAHOMA)
            .setFontSize(3)
        folioReader.setConfig(config, true)
            .openBook("file:///android_asset/abc.epub")
    }
}